<?php
/**
 * Created by PhpStorm.
 * User: HP
 * Date: 02-May-17
 * Time: 2:57 PM
 */

namespace App;


class Token
{
    protected $token;

    public function __construct($token_value = null){
        // if there is already token
        if($token_value){
            $this->token = $token_value;
        } else {
            //generate a new one
            $this->token = bin2hex(random_bytes(16)); //generate a string 16 bytes long and convert it to a string
        }


    }

    public function getValue(){
        return $this->token;
    }

    public function getHash(){
        return hash_hmac('sha256', $this->token, 'App\Config::SECRET_KEY');
    }


}