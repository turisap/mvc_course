<?php
/**
 * Created by PhpStorm.
 * User: HP
 * Date: 02-May-17
 * Time: 12:40 PM
 */

namespace App;


class Flash {

    // some constants to set up types of flash messages (for styling purposes)
    const SUCCESS = 'success';
    const WARNING = 'warning';
    const INFO    = 'info';

    // this method assigns a flash message to a SESSION var as well as its type (for styling)
    public static function addMessage($message, $type = 'success'){
        // if there is no a session with message already, create one
        if (! isset($_SESSION['flash_messages'])){
            $_SESSION['flash_messages'] = [];
        }
        // and append $message argument to it
        $_SESSION['flash_messages'][] = [
            'body' => $message,
            'type' => $type
        ];
    }

    public static function getMessages(){
        if(isset($_SESSION['flash_messages'])){
            //assign message array to a variable
            $message = $_SESSION['flash_messages'];
            // unset SESSION var in order to avoid it showing everywher after login
            unset($_SESSION['flash_messages']);
            // and return just a var
            return $message;
        }
    }
}