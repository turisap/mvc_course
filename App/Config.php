<?php

namespace App;

/**
 * Application configuration
 *
 * PHP version 5.4
 */
class Config
{

    /**
     * Database host
     * @var string
     */
    const DB_HOST = 'localhost';

    /**
     * Database name
     * @var string
     */
    const DB_NAME = 'mvclogin';

    /**
     * Database user
     * @var string
     */
    const DB_USER = 'root';

    /**
     * Database password
     * @var string
     */
    const DB_PASSWORD = '';

    /*errors dispalying only for users with authorised access to it */
    const SHOW_ERRORS = true;

    // secret string for token hashing
    const SECRET_KEY = 'X9t02JQM35A0f5j5qpDEdZ0Lj8S680sJ';

    // mailgun API key
    const MAILGUN_API_KEY = 'key-f7627760d355bedaa9becb6028bac151';

    // mailgun domain
    const MAILGUN_DOMAIN = 'sandboxd9dfed45f29840f6b68313a5d453ac0b.mailgun.org';

    // max file upload size
    const MAX_FILE_SIZE = 10000000;

}
