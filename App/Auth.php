<?php
/**
 * Created by PhpStorm.
 * User: HP
 * Date: 01-May-17
 * Time: 7:11 PM
 */

namespace App;
use App\Models\RememberedLogin;
use \App\Models\User;


class Auth
{
    static function login($user, $remember_me){
        //this line regenerate session id on log in (for safety purposes)
        session_regenerate_id(true);
        $_SESSION['user_id'] = $user->id;
        // if remember me checkbox was checked
        if($remember_me){
            // set cookie of record of a token was made
            if($user->rememberLogin()){
               setcookie('remember_me', $user->remember_token, $user->expire_timestamp, '/');
            }
        }
    }

    static function logout(){
        // Unset all of the session variables.
        $_SESSION = array();

        // If it's desired to kill the session, also delete the session cookie.
        // Note: This will destroy the session, and not just the session data!
        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 42000,
                $params["path"], $params["domain"],
                $params["secure"], $params["httponly"]
            );
        }

        // Finally, destroy the session.
        session_destroy();
        // delete all information related to automatic logginig in  through cookies
        static::forgetLogin();
    }


    // this method saves a url to a page where it is called
    public static function rememberRequestedPage(){
        $_SESSION['requested_page'] = $_SERVER['REQUEST_URI'];
    }

    // returns value of url from session of previous method or home page if there is no one
    public static function getReturnedPage(){
        return $_SESSION['requested_page'] ?? '/';
    }

    public static function getCurrentUser(){

        if(isset($_SESSION['user_id'])){
            return User::findByID($_SESSION['user_id']);

        } else {
            // login user througn cookie
            return static::loginFromRememberCookie();
        }
    }

    public static function loginFromRememberCookie(){
        $cookie = $_COOKIE['remember_me'] ?? false;
        //echo $cookie;

        if($cookie){
            //make request to the corresponding model
            $remembered_login = RememberedLogin::findByToken($cookie);
            //if token was found
            if($remembered_login && ! $remembered_login->hasExpired()){
                echo "work";
                $user = $remembered_login->getUser();
                static::login($user, false);
                return $user;
            } else {
                echo "failed";
            }
        }
    }

    // this method erases remembered login
    protected static function forgetLogin(){

        $cookie = $_COOKIE['remember_me'] ?? false;
         if($cookie){
             $remembered_login = RememberedLogin::findByToken($cookie);
             if($remembered_login){
                 $remembered_login->delete();
             }
             // and set cookie to the past
             setcookie('remember_me', time()-36000);
         }
    }



}