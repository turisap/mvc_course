<?php
/**
 * Created by PhpStorm.
 * User: HP
 * Date: 30-Apr-17
 * Time: 12:33 PM
 */

namespace App\Models;
use \App\Controllers\Account;
use App\Token;
use Core\View;
use PDO;
use \App\Mail;


class User extends \Core\Model {
    public $errors = array(); // an array for collecting errors messages

    // default value of an  empty array is for those object which are created without passing argument, f.e. this script line 97
    public function __construct($data = []) {
        // here we make object's properties and their values out of POST array
        foreach($data as $key => $value){
            $this->$key = $value;
            //echo $key;
        }
    }

    public function save(){

        // validate data first
        $this->validate();

        // if error[] array id empty, so no errors make a record in the database
        if(empty($this->errors)){
            $password_hash = password_hash($this->password, PASSWORD_DEFAULT);
            //get an activation token and it's hash
            $token = new Token();
            $token_hash = $token->getHash();
            // we assign value of an activation token to an object's property
            $this->activation_token = $token->getValue();

            $sql = 'INSERT INTO users (username, email, password_hash, activation_hash)
                     VALUES (:name, :email, :password_hash, :activation_hash)';

            $db = static::getDB();
            $statement = $db->prepare($sql);

            $statement->bindValue(':name', $this->name, PDO::PARAM_STR);
            $statement->bindValue(':email', $this->email, PDO::PARAM_STR);
            $statement->bindValue(':password_hash', $password_hash, PDO::PARAM_STR);
            $statement->bindValue(':activation_hash', $token_hash, PDO::PARAM_STR);

            return $statement->execute(); // because execute() returns true of false
        }
        return false; //if validation failed
    }

    // this method validates form data
    public function validate($id_ignore = null){

        //  THIS WAS THE BUG! ALL THIS PROPERTIES MUST BE NAMED AS COLUMNS IN THE DATABASE (but it also causes problems on signing in)
        // so there is a workaround
        if(isset($this->username)){
            if($this->username == ''){
                $this->errors[] = "Please fill the name field";
            }
        } else {
            if($this->name == ''){
                $this->errors[] = "Please fill the name field";
            }
        }

        if(static::emailExists($this->email, $id_ignore)){
            $this->errors[] = "This email is already taken";
        }

        if(filter_var($this->email, FILTER_VALIDATE_EMAIL) === false){
            $this->errors[] = "Please enter a valid email";
        }

        /*if($this->password != $this->password_confirmation){
            $this->errors[] = "Passwords should be the same";
        }*/

        // validate password only if it was set
        if(isset($this->password)){

            if(preg_match('/.*[a-z]+.*/i', $this->password) == 0){
                $this->errors[] = "Password should have at least one letter";
            }
            if(preg_match('/.*\d+./i', $this->password) == 0){
                $this->errors[] = "Password should have at least one number";
            }
        }

    }

    //check whether an email is already in the database
    // (also returns true in the case not new user but that one who change personal data)
    public static function emailExists($email, $ignore_id = null){

        $user = static::findByEmail($email);
        if($user){
            if($user->id != $ignore_id){
                return true;
            }
        }
        return false;
    }

    static function findByEmail($email){

        $sql = 'SELECT * FROM users WHERE email = :email';

        $db = static::getDB();
        $statement = $db->prepare($sql);
        $statement->bindParam(':email', $email, PDO::PARAM_STR);

        // this line makes this method to return an object instead of array
        $statement->setFetchMode(PDO::FETCH_CLASS, get_called_class());

        $statement->execute();

        return $statement->fetch();
    }

    // check the user in the database
    static function authentificate($password, $email){

        $user = static::findByEmail($email);

        // authentificate a user only if he/she activated account
        if($user && ($user->is_active)){
            if(password_verify($password, $user->password_hash)){
                return $user;
            }
        }
        return false;
    }

    static function findByID($id){

        $sql = 'SELECT * FROM users WHERE id = :id';

        $db = static::getDB();
        $statement = $db->prepare($sql);
        $statement->bindParam(':id', $id, PDO::PARAM_INT);

        // this line makes this method to return an object instead of array
        $statement->setFetchMode(PDO::FETCH_CLASS, get_called_class());

        $statement->execute();

        return $statement->fetch();
    }

    public function rememberLogin(){

        $token = new Token();
        $token_hash = $token->getHash();
        $this->remember_token = $token->getValue();
        // make timestap a property of the object
        $this->expire_timestamp = time() + 60 * 60 * 24 * 30; // expire date in 30 days time

        $sql = 'INSERT INTO remembered_login (token_hash, user_id, expires_at) VALUES (:token_hash, :user_id, :expires_at)';
        $db = static::getDB();

        $statement = $db->prepare($sql);
        $statement->bindValue(':token_hash', $token_hash, PDO::PARAM_STR);
        $statement->bindValue(':user_id', $this->id, PDO::PARAM_INT);
        $statement->bindValue(':expires_at', date('Y-m-d H-i-s', $this->expire_timestamp));

        return $statement->execute();
    }

    // send request for reseting user's password
    public static function requestPasswordSend($email){
        $user = self::findByEmail($email);
        if($user){
            if($user->startPasswordReset()){
                $user->sendResetLink();
            }
        }
    }

    // this method generates new token for password reset and writes it to the database
    protected function startPasswordReset(){
        $token = new Token();
        $token_hash = $token->getHash();
        $expire_timestamp = time() + 60 * 60 * 2; // 2 hours from now
        $this->password_reset_token = $token->getValue();

        $sql = 'UPDATE users SET password_reset_hash = :token_hash, password_reset_expire = :expires_at WHERE id = :id';
        $db = static::getDB();
        $statement = $db->prepare($sql);

        $statement->bindValue(':token_hash', $token_hash, PDO::PARAM_STR);
        $statement->bindValue('expires_at', date('Y-m-d H-i-s', $expire_timestamp), PDO::PARAM_STR);
        $statement->bindValue(':id', $this->id, PDO::PARAM_INT);

        return $statement->execute();
    }

    // this method sends link for password reset
    protected function sendResetLink(){
        $link = 'http://' . $_SERVER['HTTP_HOST'] . '/password/reset/' . $this->password_reset_token;
        $text = View::getTemplate('Password/reset_email.txt', ['url' => $link]);
        Mail::send($this->email, 'passoword@reset.ru', 'password reset', $text);
    }

    // this method finds a user by token from url (which is in email for password reset)
    public static function findByPasswordReset($token){

        // new token object by existing token
        $token = new Token($token);
        $token_hash = $token->getHash();

        $sql = 'SELECT * FROM users WHERE password_reset_hash = :token_hash';
        $db = static::getDB();
        $statement = $db->prepare($sql);

        $statement->bindValue(':token_hash', $token_hash, PDO::PARAM_STR);
        $statement->setFetchMode(PDO::FETCH_CLASS, get_called_class());
        $statement->execute();

        $user = $statement->fetch();

        // check whether token expired
        if($user){
            if(strtotime($user->password_reset_expire) > time()){
                return $user;
            }
        }
    }

    public function resetPassword($password){
        $this->password = $password;
        $this->validateResetedPassword($password);
        // if there are no errors, update the password in the database and set token and it's expire date to NULL
        if(empty($this->errors)){
            $password_hash = password_hash($this->password,PASSWORD_DEFAULT);
            $sql = 'UPDATE users SET password_hash = :password_hash, password_reset_hash = NULL,
            password_reset_expire = NULL WHERE id = :id';

            $db = static::getDB();
            $statement = $db->prepare($sql);
            $statement->bindValue(':password_hash', $password_hash, PDO::PARAM_STR);
            $statement->bindValue(':id', $this->id, PDO::PARAM_INT);

            return $statement->execute();
        }
        // if validation failed
        return false;
    }

    // checks password on the reset password page
    protected function validateResetedPassword($password){
        if(strlen($password) < 6){
            $this->errors[] = "Password should be at least 6 characters long";
        }
        if(preg_match('/.*[a-z]+.*/i', $password) == 0){
            $this->errors[] = "Password should have at least one letter";
        }
        if(preg_match('/.*\d+./i', $password) == 0){
            $this->errors[] = "Password should have at least one number";
        }
    }

    // sends email with activation link
    public function sendActivationEmail(){
        $link = 'http://' . $_SERVER['HTTP_HOST'] . '/signup/activate/' . $this->activation_token;
        $text = View::getTemplate('signup/activation_success.html', ['url' => $link]);
        Mail::send($this->email, 'activate@account.ru', 'Account activation', $text);
    }

    // this method activates account (updates is_active column to true in the users table)
    public static function activate($token){
        // create a new token object using token from the url
        $token = new Token($token);
        $token_hash = $token->getHash();

        $sql = 'UPDATE users SET is_active = 1, activation_hash = NULL WHERE activation_hash = :token_hash';
        $db = static::getDB();
        $statement = $db->prepare($sql);
        $statement->bindValue(':token_hash', $token_hash, PDO::PARAM_STR);

        $statement->execute();

    }

    // this method updates user's info
    public function update($data, $id){
        // assign data from the edit form to the object's properties
        // set password as a property only if it was supplied in the form
        if($data['password'] != ''){
            $this->password = $data['password'];
        }
        $this->name         = $data['name'];
        $this->email        = $data['email'];
        $this->check_id     = $id;

        // validate the data
        $this->validate($id);

        // if array with errors is empty
        if(empty($this->errors)){
            $sql = 'UPDATE users SET ';
            if(isset($this->password)){
                $sql .= 'password_hash = :password_hash, ';
            }
            $sql .= 'username = :name, email = :email WHERE id = :id';
            $db = static::getDB();
            $stm = $db->prepare($sql);
            if(isset($this->password)){
                $password_hash = password_hash($this->password, PASSWORD_DEFAULT);
                $stm->bindValue(':password_hash', $password_hash, PDO::PARAM_STR);
            }
            $stm->bindValue(':name', $this->name, PDO::PARAM_STR);
            $stm->bindValue(':email', $this->email, PDO::PARAM_STR);
            $stm->bindValue(':id', $this->id, PDO::PARAM_INT);

            // return true on success
            return $stm->execute();
        }
        return false; // return false on failure
    }

}