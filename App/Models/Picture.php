<?php
/**
 * Created by PhpStorm.
 * User: HP
 * Date: 13-May-17
 * Time: 3:31 PM
 */

namespace App\Models;

use PDO;
use \App\Auth;
use \App\Config;


class Picture extends \Core\Model {

    public $errors_on_upload = []; // array for saving error messages
    protected static $upload_derictory = 'public/Pictures';
    protected static $path = 'Pictures';
    protected static $db_table = 'pictures';
    public $upload_errors_array = array(


    UPLOAD_ERR_OK            => "There is no errors",
    UPLOAD_ERR_INI_SIZE      => "The uploaded file exceeds max size",
    UPLOAD_ERR_FORM_SIZE     => "The uploaded file exceeds max size of form post request",
    UPLOAD_ERR_PARTIAL       => "The uploaded file was only partially uploaded",
    UPLOAD_ERR_NO_FILE       => "No file was uploaed",
    UPLOAD_ERR_NO_TMP_DIR    => "Missing the temporary folder",
    UPLOAD_ERR_CANT_WRITE    => "Failed to write to disk",
    UPLOAD_ERR_EXTENSION     => "A php extension stopped the file upload",

    );

    // default value of an  empty array is for those object which are created without passing argument
    public function __construct($post =[], $files = []) {
        // here we make object's properties and their values out of POST array
        foreach($post as $key => $value){
            $this->$key = $value;
        }
        if(!empty($files)){
            foreach($files['picture'] as $key => $value){
                $this->$key = $value;
            }
        }

    }

    public function save(){

        // get current user
        $user = Auth::getCurrentUser();
        $name = time() . basename($this->name);

        // validate form data
        $this->validateFormData();

        if(empty($this->errors_on_upload)){
            $sql = 'INSERT INTO pictures (title, caption, filename, type, size, added_by)
                VALUES(:title, :caption, :name, :type, :size, :added_by)';
            $db = static::getDB();
            $statement = $db->prepare($sql);

            $statement->bindValue(':title', $this->title, PDO::PARAM_STR);
            $statement->bindValue(':caption', $this->caption, PDO::PARAM_STR);
            $statement->bindValue(':name', $name, PDO::PARAM_STR);
            $statement->bindValue(':type', $this->type, PDO::PARAM_STR);
            $statement->bindValue(':size', $this->size, PDO::PARAM_INT);
            $statement->bindValue(':added_by', $user->username, PDO::PARAM_STR);

            $statement->execute();

            $target_path = dirname(dirname(__DIR__)) . '/' . static::$upload_derictory . '/' . $name;
            if(file_exists($target_path)){
                $this->errors_on_upload[] = 'This file already exists in this derictory';
                return false;
            }
            if( ! move_uploaded_file($this->tmp_name, $target_path)){
                $this->errors_on_upload[] = 'The folder probaly doesnt have permissions';
            }

            return true;
        }
        return false;
    }

    public function validateFormData(){

        $extension = $this->type;

        if(strlen($this->title) < 6){
            $this->errors_on_upload[] = "Title should be at least 6 characters long";
        }
        if(strlen($this->caption) < 15){
            $this->errors_on_upload[] = "Caption should be at least 15 characters long";
        }
        if($extension != 'image/jpeg' && $extension != 'image/png' && $extension != 'image/jpg'){
            $this->errors_on_upload[] = "Your file should be .jpeg, .jpg or .png";
        }
        if(empty($this->name)){
            $this->errors_on_upload[] = "There is no file to download";
        }
        if($this->size > Config::MAX_FILE_SIZE){
            $this->errors_on_upload[] = "Your picture shouldn't be more than 10 Mb";
        }

        if($this->error != 0) { //0 means no error, so if otherwise, display a respective message
            $this->errors_on_upload[] = $this->upload_errors_array[$this->error];
        }
        //print_r($this->errors_on_upload);

    }

    // adds pictures pathes to each array in array with pictures
    public static function getAll(){
        $pictures = static::findAll();
        $path     = static::pathToUploads();
        $pictures_with_path = [];

        $i = 0;
        foreach($pictures as $picture){
            $picture['path'] = $path[$i];
            $pictures_with_path[] = $picture;
            $i++;
        }

        return $pictures_with_path;
    }

    // generates pathes to pictures files to each file
    public static function pathToUploads($id= null){
        //  if there is no id supplied create an array with pathes to all pictures
        if($id === null) {
            $pictures = static::findAll();
            $path_to_pictures = array();

            foreach ($pictures as $picture) {
                $path_to_pictures[] = '/' . self::$path . '/' . $picture['filename'];
            }
            return $path_to_pictures;

        } else {
            // if there is an id, find that picture and return its path
            $picture = self::findByID($id);
            $path = '/' . self::$path . '/' . $picture->filename;
            return $path;
        }
    }

    public static function update($id, $post){
        if($picture = static::findByID($id)){
            $sql = 'UPDATE ' . static::$db_table . ' SET title = :title, caption = :caption WHERE id = :id';
            $db = static::getDb();

            $statement = $db->prepare($sql);
            $statement->bindValue(':title', $post['title'], PDO::PARAM_STR);
            $statement->bindValue(':caption', $post['caption'], PDO::PARAM_STR);
            $statement->bindValue(':id', $picture->id, PDO::PARAM_INT);

            return $statement->execute();
        }
    }




}