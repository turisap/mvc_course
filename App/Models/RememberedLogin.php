<?php
/**
 * Created by PhpStorm.
 * User: HP
 * Date: 03-May-17
 * Time: 12:57 PM
 */

namespace App\Models;


use App\Token;
use PDO;


class RememberedLogin extends \Core\Model {

    public static function findByToken($token){
        $token = new Token($token);
        $token_hash = $token->getHash();

        $sql = 'SELECT * FROM remembered_login WHERE token_hash = :token_hash';
        $db = static::getDB();

        $stm = $db->prepare($sql);
        $stm->bindValue(':token_hash', $token_hash, PDO::PARAM_STR);
        $stm->setFetchMode(PDO::FETCH_CLASS, get_called_class());

        $stm->execute();
        return $stm->fetch();
    }

    public function getUser(){
        return User::findByID($this->user_id);
    }

    public function hasExpired(){
        return strtotime($this->expires_at) < time();
    }

    // this method deletes a token from the database
    public function delete(){
        $sql = 'DELETE FROM remembered_login WHERE token_hash = :token_hash';
        $db = static::getDB();

        $stm = $db->prepare($sql);
        $stm->bindValue(':token_hash', $this->token_hash, PDO::PARAM_STR);
        $stm->execute();
    }
}