<?php
/**
 * Created by PhpStorm.
 * User: HP
 * Date: 02-May-17
 * Time: 7:48 AM
 */

namespace App\Controllers;
use Core\View;


class Items extends Authantificated {

    // this method renders template
    public function indexAction(){
        View::renderTemplate('Items/index.html');
    }

    public function showAction(){
        echo "<h1>Hi from index show method</h1>";
    }

    public function newAction(){
        echo "<h1>Hi from index new method</h1>";
    }

}