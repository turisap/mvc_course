<?php
/**
 * Created by PhpStorm.
 * User: HP
 * Date: 13-May-17
 * Time: 11:44 AM
 */

namespace App\Controllers;
use App\Flash;
use Core\View;
use App\Models\Picture;


// extends Authantificated to get action filter to require autorization before accessing pages
class Pictures extends Authantificated {

   // action filter which requires authorization
    protected function before()
    {
        //echo 'before';
        parent::before();
    }

    protected function after()
    {
        //echo 'after';
        parent::after();
    }

    // shows all pictures in pictures/show.html
    public function showAction(){
         $pictures = Picture::getAll();
         View::renderTemplate('Pictures/show.html', ['pictures' => $pictures]);
    }

    // renders add picture page
    public function addAction(){
        View::renderTemplate('Pictures/add.html');
    }

    public function editAction(){
        $picture = Picture::findById($_GET['id']);
        $path = Picture::pathToUploads($_GET['id']);
        View::renderTemplate('Pictures/edit.html', ['picture' => $picture, 'path' => $path]);
    }

    public function addPicture(){
        //create a new picture object using all data from POST and FILES
        $picture = new Picture($_POST, $_FILES);
        //print_r($picture);

        if($picture->save()){
            Flash::addMessage('Your picture was successfully saved', Flash::SUCCESS);
            View::renderTemplate('Pictures/success.html');
        } else {
            // render template and show errors
            View::renderTemplate('/Pictures/add.html', ['picture' => $picture]);
        }
    }

    // updates a picture after editing
    public function update(){
        // if a picture has been updated successfully, render the show page
        if(Picture::update($_GET['id'], $_POST)){
            echo 'success';
            //$pictures = Picture::getAll();
            //Flash::addMessage('Photo has been updated', Flash::SUCCESS);
            //View::renderTemplate('Pictures/show.html', ['pictures' => $pictures]);
        } else {
            //otherwise,
        }
    }




}