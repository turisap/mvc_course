<?php
/**
 * Created by PhpStorm.
 * User: HP
 * Date: 11-May-17
 * Time: 12:26 PM
 */

namespace App\Controllers;


use Core\View;
use \App\Models\User;

class Password extends \Core\Controller {

    public function forgotAction(){
        View::rendertemplate('Password/forgot.html');
    }

    public static function requestResetAction(){
        User::requestPasswordSend($_POST['email']);
        View::renderTemplate('Password/reset_requested.html');
    }

    // shows the reset password page and finds a user by token
    public function resetAction(){
        // get token from the url
        $token = $this->route_parametrs['token'];
        $user = $this->getUserOrExit($token);
            // we pass the value of token to the view in order to pass it to the resetPasswordAction() method through
            // hidden input after
            View::renderTemplate('Password/reset.html', ['token' => $token]);

    }

    // this method reseting the password
    public function resetPasswordAction(){
        // get token from the view
        $token = $_POST['token'];
        $user = $this->getUserOrExit($token);
        if($user->resetPassword($_POST['password'])){
            View::renderTemplate('password/reset_success.html');
        } else {
            // we pass errors array with messages to display via $user (user object)
            View::renderTemplate('password/reset.html', ['token' => $token, 'user' => $user]);
        }

    }

    protected function getUserOrExit($token){
        $user = User::findByPasswordReset($token);
        if($user){
            return $user;
        } else {
            View::renderTemplate('password/token_expired.html');
            exit;
        }
    }

}