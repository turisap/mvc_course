<?php
/**
 * Created by PhpStorm.
 * User: HP
 * Date: 02-May-17
 * Time: 9:22 AM
 */

namespace App\Controllers;


abstract class Authantificated extends \Core\Controller {


    // this is an action filter. It runs before all methods in this controller and virtually checks if a user is logged in
    // it will be used in all controllers which requires so but will be kept here
    protected function before(){
        // check whether a user is logged in
        $this->requireAuthorization();
    }
}