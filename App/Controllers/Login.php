<?php
/**
 * Created by PhpStorm.
 * User: HP
 * Date: 01-May-17
 * Time: 3:23 PM
 */

namespace App\Controllers;
use App\Flash;
use \Core\View;
use App\Models\User;
use App\Auth;


class Login extends \Core\Controller {

    // this method just shows the login page
    public function newAction(){
        View::renderTemplate('Login/new.html');
    }

    //this method logs user in
    public function createAction(){
        //echo $_REQUEST['email'] . ',' . $_REQUEST['password']; //just to test it out
        $user = User::authentificate($_POST['password'], $_POST['email']);
        //var_dump($user);
        //var_dump($_POST);
        $remember_me = isset($_POST['remember_me']); // for checking back the checkbox on unsuccessful login

        // if authentification is ok, redirect user to the home page and to the login page otherwise
        if($user){
            Auth::login($user, $remember_me);
            Flash::addMessage('Successfully logged in', Flash::SUCCESS);
            //redirect user to a page where he was before redirecting to the login page
            $this->redirect(Auth::getReturnedPage());
        } else {
            Flash::addMessage('Unsuccessful', Flash::WARNING);
            View::renderTemplate('Login/new.html', [
                'email' => $_POST['email'],
                'remember_me' => $remember_me //passing it back to the View
                ]);
        }
    }

    public function destroy(){
        Auth::logout();
        // we cannot put message after this line because by this sessions are destroyed, so we put it in the next method
        // and redirect to it
        $this->redirect('/login/display-logout-message');

    }

    public function displayLogoutMessage(){
        //here, a new session is already started
        Flash::addMessage('Successfully logged out', Flash::SUCCESS);
        $this->redirect('/');
    }

}