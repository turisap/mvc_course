<?php
/**
 * Created by PhpStorm.
 * User: HP
 * Date: 12-May-17
 * Time: 2:50 PM
 */

namespace App\Controllers;


use \App\Auth;
use App\Flash;
use Core\View;

// we extend this Authantificated class because it makes the user to login before access the page
class Profile extends Authantificated {

    // this is an action filter. We use Auth::gerCurrentUser() in all methods in this class, so we put it in the before () method
    // and we elliminated duplicated code by doing that
    protected function before(){
        // calling before() method of the parent class (because before() of this class overrides before() of the parent,
        //switching off requiring of authorization
        parent::before();
        // make a user object this object's property
        $this->user = Auth::getCurrentUser();
    }

    // render template of the show page
    public function showAction(){
        // get the user object with all info (from cookie or session) - now in before method
        //$user = Auth::getCurrentUser();
        // pass the user object to the view while rendering template
        View::renderTemplate('Profile/show.html', ['user' => $this->user]);
    }

    // render template of the edit page
    public function editAction(){
        // get the user object with all info (from cookie or session)- now in before method
        //$user = Auth::getCurrentUser();
        // pass the user object to the view while rendering template
        View::renderTemplate('Profile/add.html', ['user' => $this->user]);
    }

    public function updateAction(){
        // get the current user from session or cookies - now in before method
        //$user = Auth::getCurrentUser();
        // this id for emailExist() ignore
        $user_id = $this->user->id;

        // try update user using data from the form
        if($this->user->update($_POST, $user_id)){

            // success message
            Flash::addMessage('Your profile has been updated successfully');
            $this->redirect('/profile/show');

        } else {
            // redisplay the edit page with error messages in user object
            View::renderTemplate('Profile/add.html', ['user' => $this->user]);
        }
    }

}