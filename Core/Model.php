<?php

namespace Core;

use PDO;
use App\Config;
/**
 * Base model
 *
 * PHP version 5.4
 */
abstract class Model
{

    /**
     * Get the PDO database connection
     *
     * @return mixed
     */
    protected static function getDB()
    {
        static $db = null;

        if ($db === null) {

            $dsn = 'mysql:host=' . Config::DB_HOST . ';dbname=' .
                Config::DB_NAME . ';charset=utf8';
            $db = new PDO($dsn, Config::DB_USER, Config::DB_PASSWORD);

            // Throw an exception when an error occurs
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        }
        return $db; // this was inside the previous if statement and caused a SQL error
    }

    public static function findAll(){

        $sql = 'SELECT * FROM ' . static::$db_table ;

        $db = static::getDB();
        $statement = $db->prepare($sql);
        $statement->setFetchMode(PDO::FETCH_CLASS, get_called_class());
        $statement->execute();

        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    public static function findByID($id){

        $sql = 'SELECT * FROM ' . static::$db_table . ' WHERE id= :id';
        $db = static::getDB();

        $statement = $db->prepare($sql);
        $statement->bindValue(':id', $id, PDO::PARAM_INT);
        $statement->setFetchMode(PDO::FETCH_CLASS, get_called_class());
        $statement->execute();

        return $statement->fetch();
    }























}
